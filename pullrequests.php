<?php
	include_once("CommandToolsClass.php");

    $countInputArgument=$argc;

    switch ($countInputArgument) {
        case 2:
            $command = new CommandTools();
            $nameuser=$argv[1];
            $command->getRepository($nameuser);
            break;
        case 3:
            $command = new CommandTools();
            $nameuser=$argv[1];
            $reponame=$argv[2];
            $command->getPullRequest($nameuser,$reponame);
            break;
        default:
            $command = new CommandTools();
            $command->WriteLine(' ');
            break;
    }

?>