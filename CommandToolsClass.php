<?php

/**
 * Command Line Tool that searches for those PullRequests in a public repository
 *
 *
 * @author ILGIZ <i.torgoev@gmail.com>
 * @version 1.0
 */
class CommandTools
{

    /**
     *
     * Get Repository list and print;
     *
     * @param string $nameuser Username Bitbucket
     *
     *
     */
    public function getRepository($nameuser)
    {
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, 'https://api.bitbucket.org/2.0/repositories/'.$nameuser);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $json = curl_exec($curl);
            curl_close($curl);

            $json = json_decode( $json ); // json

            $this->checkResult($json);

            $this->printRepository($json);
        }else{
            $this->writeline("Error connect API ");
        }
    }

    /**
     *
     * Get PullRequest list and print;
     *
     * @param string $nameuser Username Bitbucket
     * @param string $reponame Repository Name Bitbucket
     *
     *
     */
    public function getPullRequest($nameuser , $reponame)
    {
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, 'https://api.bitbucket.org/2.0/repositories/'.$nameuser.'/'.$reponame.'/pullrequests');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $json = curl_exec($curl);
            curl_close($curl);

            $json = json_decode( $json ); // json

            $this->checkResult($json);

            $this->printPullRequest($json);
        }else{
            $this->writeline("Error connect API ");
        }
    }

    /**
     *
     * print Repository list and link;
     *
     * @param string $json Response Bitbucket API
     *
     *
     */
    public function printRepository($json)
    {

        $this->writeline("List of Repository ");
        $nn=1;
        foreach ($json->values as $value) {
             $this->writeline($nn.". ".$value->name."\n"."  ".$value->links->html->href);
             $nn++;
        }
    }

    /**
     *
     * print PullRequest list and link;
     *
     * @param string $json Response Bitbucket API
     *
     *
     */
    public function printPullRequest($json)
    {
        $this->writeline("List of Pull Requests ");
        $nn=1;
        foreach ($json->values as $value) {
             $this->writeline($nn.". ".$value->title."\n"."  ".$value->links->html->href);
             $nn++;
        }
    }

    /**
     *
     * check Response json from Bitbucket API;
     *
     * @param string $json Response Bitbucket API
     *
     *
     */
    public function checkResult($json)
    {
        if (!$json){
            $this->WriteLine("  Error: not response");
            exit();
        }
        if (isset($json->type) || ($json->type=='error')){
                $this->WriteLine("  Error: ".$json->error->message);
                exit();
            }
    }

    /**
     *
     * Print beautiful line text
     *
     * @param string $text
     *
     *
     */
    public function WriteLine($text)
    {
        print_r("\n");
        print_r($text."\n");
        print_r("\n");
    }
}
?>